package net.thearcanebrony.retrostuff.worldfeatures.generators;

import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.structure.StructureManager;
import net.minecraft.structure.StructurePiece;
import net.minecraft.structure.StructurePieceType;
import net.minecraft.structure.StructurePieceWithDimensions;
import net.minecraft.util.BlockRotation;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockBox;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.ChunkRandom;
import net.minecraft.world.gen.StructureAccessor;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.thearcanebrony.retrostuff.RetroStuff;

import java.util.List;
import java.util.Random;

public class BrickPyramidGenerator extends StructurePieceWithDimensions {
    private static final Identifier PYRAMID = new Identifier("brick_pyramid");

    public BrickPyramidGenerator(StructurePieceType type, int x, int y, int z, int width, int height, int depth, Direction orientation) {
        super(type, x, y, z, width, height, depth, orientation);
    }

    public BrickPyramidGenerator(ChunkRandom random, int x, int z) {
        super((StructurePieceType) PYRAMID, x, 64, z, 64, 64, 64, Direction.NORTH);
    }

    public BrickPyramidGenerator(ServerWorld serverWorld, NbtCompound nbt) {
        super(RetroStuff.BRICK_PYRAMID, nbt);
    }

    @Override
    protected void writeNbt(ServerWorld world, NbtCompound nbt) {
        super.writeNbt(world, nbt);
    }

    public boolean generate(StructureWorldAccess access, StructureAccessor structureAccessor, ChunkGenerator chunkGenerator, Random random, BlockBox boundingBox, ChunkPos chunkPos, BlockPos pos){
        System.out.println("Pyramid generated!");
        return true;
    }
}
