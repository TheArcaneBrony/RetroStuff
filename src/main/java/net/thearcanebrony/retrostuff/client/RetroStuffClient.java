package net.thearcanebrony.retrostuff.client;

import net.fabricmc.api.ClientModInitializer;
import net.minecraft.SharedConstants;
import net.minecraft.client.MinecraftClient;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.registry.DynamicRegistryManager;
import net.minecraft.world.gen.GeneratorOptions;

@net.fabricmc.api.Environment(net.fabricmc.api.EnvType.CLIENT)
public class RetroStuffClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {

    }
}
