package net.thearcanebrony.retrostuff.mixin.client;

import net.minecraft.advancement.criterion.CriterionConditions;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.server.RecipesProvider;
import net.minecraft.data.server.recipe.RecipeJsonProvider;
import net.minecraft.data.server.recipe.ShapedRecipeJsonFactory;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.Items;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.function.Consumer;

@Mixin(RecipesProvider.class)
public abstract class RecipesProviderMixin {

//    @Shadow private static CriterionConditions conditionsFromItem(ItemConvertible item);

    @Inject(method = "generate", at = @At("TAIL"), cancellable = true)
    private static void generate(Consumer<RecipeJsonProvider> exporter, CallbackInfo ci){
//        ShapedRecipeJsonFactory.create(Items.ENCHANTED_GOLDEN_APPLE).input('#', (ItemConvertible)Items.GOLD_BLOCK).input('X', (ItemConvertible)Items.APPLE).pattern("###").pattern("#X#").pattern("###").criterion("has_gold_block", conditionsFromItem(Items.GOLD_INGOT)).offerTo(exporter);
    }
}
