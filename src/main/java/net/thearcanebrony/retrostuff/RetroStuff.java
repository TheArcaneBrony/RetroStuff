package net.thearcanebrony.retrostuff;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.structure.v1.FabricStructureBuilder;
import net.minecraft.structure.StructurePieceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.BuiltinRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.feature.ConfiguredStructureFeature;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;
import net.minecraft.world.gen.feature.StructureFeature;
import net.thearcanebrony.retrostuff.worldfeatures.BrickPyramid;
import net.thearcanebrony.retrostuff.worldfeatures.generators.BrickPyramidGenerator;

public class RetroStuff implements ModInitializer {
    public static final StructurePieceType BRICK_PYRAMID = BrickPyramidGenerator::new;
    private static final StructureFeature<DefaultFeatureConfig> BRICK_PYRAMID_ = new BrickPyramid(DefaultFeatureConfig.CODEC);
    private static final ConfiguredStructureFeature<?, ?> MY_CONFIGURED = BRICK_PYRAMID_.configure(DefaultFeatureConfig.DEFAULT);


    @Override
    public void onInitialize() {
        ConfigMgr.Get();
        ConfigMgr.Save();
        System.out.println("Hi from RetroStuff (config successfully loaded)!");
        //register structure
        Registry.register(Registry.STRUCTURE_PIECE, new Identifier("retrostuff","brick_pyramid"), BRICK_PYRAMID);
        FabricStructureBuilder.create(new Identifier("retrostuff", "brick_pyramid"), BRICK_PYRAMID_)
                .step(GenerationStep.Feature.SURFACE_STRUCTURES)
                .defaultConfig(4, 1, 12345)
                .adjustsSurface()
                .register();
        RegistryKey<ConfiguredStructureFeature<?, ?>> myConfigured = RegistryKey.of(Registry.CONFIGURED_STRUCTURE_FEATURE_KEY,
                new Identifier("retrostuff", "brick_pyramid"));
        BuiltinRegistries.add(BuiltinRegistries.CONFIGURED_STRUCTURE_FEATURE, myConfigured.getValue(), MY_CONFIGURED);
        System.out.println("[RetroStuff] registered structures");
    }
}
